#ifndef CC1120_HPP
#define CC1120_HPP

#define masterSource 1
#define slaveSource 2
using namespace std;

typedef enum _regaccess_
{
    StdRegSpace, // Standart Register Space
    ExtAddr,     // Extended Register Space
    CmdStrobe,   // Command Strobe
    DirectFIFO,  // Direct FIFO Access
    StdFIFO      // Standart FIFO access
} RegAcces;

class radiocc1120
{
private:
    // Variable for config
    unsigned int syncWord;

    // Config Function
    void configSyncWord();

    // Variable for All function
    unsigned char recv_data[200];
    float timeOut = 2;

    // Variable for initFunction
    unsigned char freqSet;
    unsigned char txPowerSet;
    unsigned char chipSelectPin;
    unsigned char chipResetPin;
    unsigned char chipIrqPin;

public:
    radiocc1120();
    ~radiocc1120();
    static const int channel = 0;
    unsigned char bufferResult;
    unsigned char status;
    int pqtCounter[50]; // for 51 slave nodes

    // GUI Function
    int getTxPower();
    void setSniffMode();
    void setSyncWord(unsigned int value); //SYNC WORD Value (32-bit) default 0x930B51DE
    void setRegister(unsigned short int  addr, unsigned char value);
    void setChipAddress(unsigned char chipAddr);
    float getCarrierFreq();

    // Cc1120 Function
    void registerSetCc1120();
    void spiInitCc1120(unsigned char cs, unsigned char rst, unsigned char irq);
    void cc1120HwReset();
    void errorHandler();
    void flushRxFifo(); // SFRX Command Strobe
    void flushTxFifo(); // SFTX Command Strobe
    void send(unsigned char *pData, int len);
    void recv();
    void readPacket(unsigned char *pData, int *pLen, char *pRssi, char *pLQI, bool *pCrc);
    void txPacketHandler(unsigned char *pData, unsigned char length, unsigned char chipAddr);
    int packetCounter(unsigned char devAddr);
    int radioStatusCheck();
    int readRxFirst();
    int readRxLast();

    // Write/Read SPI function
    void writeRegSingle(RegAcces access, unsigned char Value, unsigned char Addr);
    void writeRegBurst(RegAcces access, unsigned char *pData, int length, unsigned char Addr);
    int readRegSingle(RegAcces access, unsigned char Addr);
    void readRegBurst(unsigned char *pData, int rxFirst, int rxLast); // for now Just for Standart FIFO Access
};

#endif //CC1120
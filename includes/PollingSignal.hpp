#ifndef POLLINGSIGNAL_HPP
#define POLLINGSIGNAL_HPP
#include <map>

using namespace std;

class PollingSignalClient {
    public:
        virtual void execPoll() = 0;
};
class PollingSignal
{
public:
    PollingSignal();
    virtual ~PollingSignal();
    void registerHandler(int signum, PollingSignalClient *handler);
    static void realPollingSignalHdlr(int signum);
    static map<int,PollingSignalClient *> handlers;

};

#endif // POLLINGSIGNAL_HPP
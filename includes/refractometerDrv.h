#ifndef REFRACTOMETERDRV_H
#define REFRACTOMETERDRV_H

#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <stdint.h>
#include <iostream>

#include <wiringPi.h>
#include <wiringSerial.h>

class refractometerDrv
{
private:
    unsigned char devId = 1;
    int fd;
    float concentration;
    float temp;
    /* Data Variable */
    int a;
    int dataBuff[25];
    unsigned char dataBuff1[25];

public:
    void serialInit();
    void readConcentrationSingle(float *pConcentration, float *pTemp);
    void userZeroSettingCalibration();
};

#endif //REFRACTOMETERDRV_H
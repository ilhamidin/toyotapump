#ifndef MQTT_LIB_H
#define MQTT_LIB_H

#include <MQTTClient.h>
#include <string>
#include <log4cplus/helpers/loglog.h>
#include <log4cplus/logger.h>
#include <stdio.h>
#include <mutex>

#include "customexception.h"

using namespace std;
using namespace log4cplus::helpers;

class MQTTLib
{
public:
    MQTTLib();
    ~MQTTLib();

    // Methods
    void initMQTT(MQTTClient_connectionLost *cl, MQTTClient_messageArrived *ma,
                  MQTTClient_deliveryComplete *dc);
    void pubMQTT(uint8_t *_payload, int _lenght, const char *_topic, int _qos = 1, int _retained = 0);
    int initSubscribe(const char *_topic, int _qos = 1);
    int stopSubscribe(const char *_topic);
    int stopMQTT();

    void setServerAddr(const char *_serverAddress);
    void setClientID(const char *_clientID);
    void setUsername(const char *_username);
    void setPassword(const char *_password);
    void setAliveInterval(int _interval);
    void setCleanSession(int _session);
    void setTimeout(int _timeout);

private:
    MQTTClient client_;
    MQTTClient_connectOptions connectOpts_;
    MQTTClient_message pubmsg_;
    MQTTClient_deliveryToken token_;

    string serverAddress_;
    string clientID_;
    string username_;
    string password_;
    int aliveInterval_;
    int cleanSession_;
    int timeout_;
    int rc_;
    mutex mutex_;
};
#endif // MQTT_LIB_H
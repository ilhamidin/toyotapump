#ifndef PHTEMPDRIVER_H
#define PHTEMPDRIVER_H

#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <stdint.h>

#include <wiringPi.h>
#include <wiringSerial.h>

class phtempdriver
{
private:
    unsigned char devId = 1;
    int fd;

    // unsigned char changeDevIdCmd[] = {devId};
    int dataBuff[25];
    unsigned char dataBuff1[10];
    unsigned char a;
    bool status;
    int buff;
    int buff1;
    uint16_t crcBuff;
    float tempBuff;
    float tempBuff1;
    float result;
    float phBuff;
    float phBuff1;

public:
    void serialInit();
    void readPhTempSingle(float *ph, float *temperature, bool *pCrc);
    void readPhTempBurst(float *ph, float *temperature, bool *pCrc, int *pIteration);
    void changeDevId(unsigned char firstAddr, unsigned char finalAddr);
    uint16_t modRtuCrc(unsigned char *buff, int len);
};

#endif //PHTEMPDRIVER
#include <iostream>
#include <thread>
#include <chrono>
#include <functional> 

class Timer {
private:
    bool active = false;

public:
    void setTimeout(std::function<void()> const& function, int delay);
    void setInterval(std::function<void()> const& function, int interval);
    void stop();
};
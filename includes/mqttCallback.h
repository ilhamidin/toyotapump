#ifndef MQTT_CALLBACK_H
#define MQTT_CALLBACK_H

#include "mqttlib.h"
#include <string>

class MQTTCallback
{
public:
	virtual void publishDone(MQTTClient_deliveryToken dt) = 0;
	virtual void recvMessage(const string _message, const string _topic) = 0;
    virtual void connectionLost() = 0;
};

void setClient(MQTTCallback *client);
void connlost(void *context, char *cause);
void delivered(void *context, MQTTClient_deliveryToken dt);
int msgarrvd(void *context, char *topicName, int topicLen, MQTTClient_message *message);

#endif // MQTT_CALLBACK_H
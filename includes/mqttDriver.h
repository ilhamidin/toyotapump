#ifndef MQTT_DRIVER_H
#define MQTT_DRIVER_H

#include "mqttCallback.h"
#include "mqttlib.h"
#include <syslog.h> // JournalCtl log Printf

class mqttDriver : public MQTTCallback
{
public:
    mqttDriver();
    ~mqttDriver();
    void publishDone(MQTTClient_deliveryToken dt);
    void recvMessage(const string _message, const string _topic);
    void connectionLost();

    MQTTLib *pMQTT_;
};
#endif // MQTT_DRIVER
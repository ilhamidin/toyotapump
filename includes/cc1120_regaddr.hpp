#ifndef REGADDRESS_HPP
#define REGADDRESS_HPP

// Standart Register Config Address
#define CC112X_IOCFG3 0x00
#define CC112X_IOCFG2 0x01
#define CC112X_IOCFG1 0x02
#define CC112X_IOCFG0 0x03
#define CC112X_SYNC3 0x04
#define CC112X_SYNC2 0x05
#define CC112X_SYNC1 0x06
#define CC112X_SYNC0 0x07
#define CC112X_SYNC_CFG1 0x08
#define CC112X_SYNC_CFG0 0x09
#define CC112X_DEVIATION_M 0x0A
#define CC112X_MODCFG_DEV_E 0x0B
#define CC112X_DCFILT_CFG 0x0C
#define CC112X_PREAMBLE_CFG1 0x0D
#define CC112X_PREAMBLE_CFG0 0x0E
#define CC112X_FREQ_IF_CFG 0x0F
#define CC112X_IQIC 0x10
#define CC112X_CHAN_BW 0x11
#define CC112X_MDMCFG1 0x12
#define CC112X_MDMCFG0 0x13
#define CC112X_SYMBOL_RATE2 0x14
#define CC112X_SYMBOL_RATE1 0x15
#define CC112X_SYMBOL_RATE0 0x16
#define CC112X_AGC_REF 0x17
#define CC112X_AGC_CS_THR 0x18
#define CC112X_AGC_GAIN_ADJUST 0x19
#define CC112X_AGC_CFG3 0x1A
#define CC112X_AGC_CFG2 0x1B
#define CC112X_AGC_CFG1 0x1C
#define CC112X_AGC_CFG0 0x1D
#define CC112X_FIFO_CFG 0x1E
#define CC112X_DEV_ADDR 0x1F
#define CC112X_SETTLING_CFG 0x20
#define CC112X_FS_CFG 0x21
#define CC112X_WOR_CFG1 0x22
#define CC112X_WOR_CFG0 0x23
#define CC112X_WOR_EVENT0_MSB 0x24
#define CC112X_WOR_EVENT0_LSB 0x25
#define CC112X_PKT_CFG2 0x26
#define CC112X_PKT_CFG1 0x27
#define CC112X_PKT_CFG0 0x28
#define CC112X_RFEND_CFG1 0x29
#define CC112X_RFEND_CFG0 0x2A
#define CC112X_PA_CFG2 0x2B
#define CC112X_PA_CFG1 0x2C
#define CC112X_PA_CFG0 0x2D
#define CC112X_PKT_LEN 0x2E

// Extended Register Config Address (+ 0x2F)
#define CC112X_IF_MIX_CFG 0x00
#define CC112X_FREQOFF_CFG 0x01
#define CC112X_TOC_CFG 0x02
#define CC112X_MARC_SPARE 0x03
#define CC112X_ECG_CFG 0x04
#define CC112X_CFM_DATA_CFG 0x05
#define CC112X_EXT_CTRL 0x06
#define CC112X_RCCAL_FINE 0x07
#define CC112X_RCCAL_COARSE 0x08
#define CC112X_RCCAL_OFFSET 0x09
#define CC112X_FREQOFF1 0x0A
#define CC112X_FREQOFF0 0x0B
#define CC112X_FREQ2 0x0C
#define CC112X_FREQ1 0x0D
#define CC112X_FREQ0 0x0E
#define CC112X_IF_ADC2 0x0F
#define CC112X_IF_ADC1 0x10
#define CC112X_IF_ADC0 0x11
#define CC112X_FS_DIG1 0x12
#define CC112X_FS_DIG0 0x13
#define CC112X_FS_CAL3 0x14
#define CC112X_FS_CAL2 0x15
#define CC112X_FS_CAL1 0x16
#define CC112X_FS_CAL0 0x17
#define CC112X_FS_CHP 0x18
#define CC112X_FS_DIVTWO 0x19
#define CC112X_FS_DSM1 0x1A
#define CC112X_FS_DSM0 0x1B
#define CC112X_FS_DVC1 0x1C
#define CC112X_FS_DVC0 0x1D
#define CC112X_FS_LBI 0x1E
#define CC112X_FS_PFD 0x1F
#define CC112X_FS_PRE 0x20
#define CC112X_FS_REG_DIV_CML 0x21
#define CC112X_FS_SPARE 0x22
#define CC112X_FS_VCO4 0x23
#define CC112X_FS_VCO3 0x24
#define CC112X_FS_VCO2 0x25
#define CC112X_FS_VCO1 0x26
#define CC112X_FS_VCO0 0x27
#define CC112X_GBIAS6 0x28
#define CC112X_GBIAS5 0x29
#define CC112X_GBIAS4 0x2A
#define CC112X_GBIAS3 0x2B
#define CC112X_GBIAS2 0x2C
#define CC112X_GBIAS1 0x2D
#define CC112X_GBIAS0 0x2E
#define CC112X_IFAMP 0x2F
#define CC112X_LNA 0x30
#define CC112X_RXMIX 0x31
#define CC112X_XOSC5 0x32
#define CC112X_XOSC4 0x33
#define CC112X_XOSC3 0x34
#define CC112X_XOSC2 0x35
#define CC112X_XOSC1 0x36
#define CC112X_XOSC0 0x37
#define CC112X_ANALOG_SPARE 0x38
#define CC112X_PA_CFG3 0x39
#define CC112X_IRQ0M 0x3F
#define CC112X_IRQ0F 0x40

// Status Register
#define CC112X_WOR_TIME1 0x64
#define CC112X_WOR_TIME0 0x65
#define CC112X_WOR_CAPTURE1 0x66
#define CC112X_WOR_CAPTURE0 0x67
#define CC112X_BIST 0x68
#define CC112X_DCFILTOFFSET_I1 0x69
#define CC112X_DCFILTOFFSET_I0 0x6A
#define CC112X_DCFILTOFFSET_Q1 0x6B
#define CC112X_DCFILTOFFSET_Q0 0x6C
#define CC112X_IQIE_I1 0x6D
#define CC112X_IQIE_I0 0x6E
#define CC112X_IQIE_Q1 0x6F
#define CC112X_IQIE_Q0 0x70
#define CC112X_RSSI1 0x71
#define CC112X_RSSI0 0x72
#define CC112X_MARCSTATE 0x73
#define CC112X_LQI_VAL 0x74
#define CC112X_PQT_SYNC_ERR 0x75
#define CC112X_DEM_STATUS 0x76
#define CC112X_FREQOFF_EST1 0x77
#define CC112X_FREQOFF_EST0 0x78
#define CC112X_AGC_GAIN3 0x79
#define CC112X_AGC_GAIN2 0x7A
#define CC112X_AGC_GAIN1 0x7B
#define CC112X_AGC_GAIN0 0x7C
#define CC112X_CFM_RX_DATA_OUT 0x7D
#define CC112X_CFM_TX_DATA_IN 0x7E
#define CC112X_ASK_SOFT_RX_DATA 0x7F
#define CC112X_RNDGEN 0x80
#define CC112X_MAGN2 0x81
#define CC112X_MAGN1 0x82
#define CC112X_MAGN0 0x83
#define CC112X_ANG1 0x84
#define CC112X_ANG0 0x85
#define CC112X_CHFILT_I2 0x86
#define CC112X_CHFILT_I1 0x87
#define CC112X_CHFILT_I0 0x88
#define CC112X_CHFILT_Q2 0x89
#define CC112X_CHFILT_Q1 0x8A
#define CC112X_CHFILT_Q0 0x8B
#define CC112X_GPIO_STATUS 0x8C
#define CC112X_FSCAL_CTRL 0x8D
#define CC112X_PHASE_ADJUST 0x8E
#define CC112X_PARTNUMBER 0x8F
#define CC112X_PARTVERSION 0x90
#define CC112X_SERIAL_STATUS 0x91
#define CC112X_MODEM_STATUS1 0x92
#define CC112X_MODEM_STATUS0 0x93
#define CC112X_MARC_STATUS1 0x94
#define CC112X_MARC_STATUS0 0x95
#define CC112X_PA_IFAMP_TEST 0x96
#define CC112X_FSRF_TEST 0x97
#define CC112X_PRE_TEST 0x98
#define CC112X_PRE_OVR 0x99
#define CC112X_ADC_TEST 0x9A
#define CC112X_DVC_TEST 0x9B
#define CC112X_ATEST 0x9C
#define CC112X_ATEST_LVDS 0x9D
#define CC112X_ATEST_MODE 0x9E
#define CC112X_XOSC_TEST1 0x9F
#define CC112X_XOSC_TEST0 0xA0

#define CC112X_RXFIRST 0xD2
#define CC112X_TXFIRST 0xD3
#define CC112X_RXLAST 0xD4
#define CC112X_TXLAST 0xD5
#define CC112X_NUM_TXBYTES 0xD6 /* Number of bytes in TXFIFO */
#define CC112X_NUM_RXBYTES 0xD7 /* Number of bytes in RXFIFO */
#define CC112X_FIFO_NUM_TXBYTES 0xD8
#define CC112X_FIFO_NUM_RXBYTES 0xD9

// Command strobe registers
#define CC112X_SRES 0x30    /*  SRES    - Reset chip. */
#define CC112X_SFSTXON 0x31 /*  SFSTXON - Enable and calibrate frequency synthesizer. */
#define CC112X_SXOFF 0x32   /*  SXOFF   - Turn off crystal oscillator. */
#define CC112X_SCAL 0x33    /*  SCAL    - Calibrate frequency synthesizer and turn it off. */
#define CC112X_SRX 0x34     /*  SRX     - Enable RX. Perform calibration if enabled. */
#define CC112X_STX 0x35     /*  STX     - Enable TX. If in RX state, only enable TX if CCA passes. */
#define CC112X_SIDLE 0x36   /*  SIDLE   - Exit RX / TX, turn off frequency synthesizer. */
#define CC112X_SWOR 0x38    /*  SWOR    - Start automatic RX polling sequence (Wake-on-Radio) */
#define CC112X_SPWD 0x39    /*  SPWD    - Enter power down mode when CSn goes high. */
#define CC112X_SFRX 0x3A    /*  SFRX    - Flush the RX FIFO buffer. */
#define CC112X_SFTX 0x3B    /*  SFTX    - Flush the TX FIFO buffer. */
#define CC112X_SWORRST 0x3C /*  SWORRST - Reset real time clock. */
#define CC112X_SNOP 0x3D    /*  SNOP    - No operation. Returns status byte. */
#define CC112X_AFC 0x37     /*  AFC     - Automatic Frequency Correction */

// Register Access 
#define RADIO_BURST_ACCESS   0x40
#define RADIO_SINGLE_ACCESS  0x00
#define RADIO_READ_ACCESS    0x80
#define RADIO_WRITE_ACCESS   0x00
#define RADIO_EXT_REG_ACCESS 0x2F

typedef enum _registerSetting_
{
   CC1120_IOCFG3,              // 0x00
   CC1120_IOCFG2,              // 0x01
   CC1120_IOCFG1,              // 0x02
   CC1120_IOCFG0,              // 0x03
   CC1120_SYNC3,               // 0x04
   CC1120_SYNC2,               // 0x05
   CC1120_SYNC1,               // 0x06
   CC1120_SYNC0,               // 0x07
   CC1120_SYNC_CFG1,           // 0x08
   CC1120_SYNC_CFG0,           // 0x09
   CC1120_DEVIATION_M,         // 0x0A
   CC1120_MODCFG_DEV_E,        // 0x0B
   CC1120_DCFILT_CFG,          // 0x0C
   CC1120_PREAMBLE_CFG1,       // 0x0D
   CC1120_PREAMBLE_CFG0,       // 0x0E
   CC1120_FREQ_IF_CFG,         // 0x0F
   CC1120_IQIC,                // 0x10
   CC1120_CHAN_BW,             // 0x11
   CC1120_MDMCFG1,             // 0x12
   CC1120_MDMCFG0,             // 0x13
   CC1120_SYMBOL_RATE2,        // 0x14
   CC1120_SYMBOL_RATE1,        // 0x15
   CC1120_SYMBOL_RATE0,        // 0x16
   CC1120_AGC_REF,             // 0x17
   CC1120_AGC_CS_THR,          // 0x18
   CC1120_AGC_GAIN_ADJUST,     // 0x19
   CC1120_AGC_CFG3,            // 0x1A
   CC1120_AGC_CFG2,            // 0x1B
   CC1120_AGC_CFG1,            // 0x1C
   CC1120_AGC_CFG0,            // 0x1D
   CC1120_FIFO_CFG,            // 0x1E
   CC1120_DEV_ADDR,            // 0x1F
   CC1120_SETTLING_CFG,        // 0x20
   CC1120_FS_CFG,              // 0x21
   CC1120_WOR_CFG1,            // 0x22
   CC1120_WOR_CFG0,            // 0x23
   CC1120_WOR_EVENT0_MSB,      // 0x24
   CC1120_WOR_EVENT0_LSB,      // 0x25
   CC1120_PKT_CFG2,            // 0x26
   CC1120_PKT_CFG1,            // 0x27
   CC1120_PKT_CFG0,            // 0x28
   CC1120_RFEND_CFG1,          // 0x29
   CC1120_RFEND_CFG0,          // 0x2A
   CC1120_PA_CFG2,             // 0x2B
   CC1120_PA_CFG1,             // 0x2C
   CC1120_PA_CFG0,             // 0x2D
   CC1120_PKT_LEN,             // 0x2E
   CC1120_IF_MIX_CFG = 0x2F00, // 0x2F00
   CC1120_FREQOFF_CFG,         // 0x2F01
   CC1120_TOC_CFG,             // 0x2F02
   CC1120_MARC_SPARE,          // 0x2F03
   CC1120_ECG_CFG,             // 0x2F04
   CC1120_CFM_DATA_CFG,        // 0x2F05
   CC1120_EXT_CTRL,            // 0x2F06
   CC1120_RCCAL_FINE,          // 0x2F07
   CC1120_RCCAL_COARSE,        // 0x2F08
   CC1120_RCCAL_OFFSET,        // 0x2F09
   CC1120_FREQOFF1,            // 0x2F0A
   CC1120_FREQOFF0,            // 0x2F0B
   CC1120_FREQ2,               // 0x2F0C
   CC1120_FREQ1,               // 0x2F0D
   CC1120_FREQ0,               // 0x2F0E
   CC1120_IF_ADC2,             // 0x2F0F
   CC1120_IF_ADC1,             // 0x2F10
   CC1120_IF_ADC0,             // 0x2F11
   CC1120_FS_DIG1,             // 0x2F12
   CC1120_FS_DIG0,             // 0x2F13
   CC1120_FS_CAL3,             // 0x2F14
   CC1120_FS_CAL2,             // 0x2F15
   CC1120_FS_CAL1,             // 0x2F16
   CC1120_FS_CAL0,             // 0x2F17
   CC1120_FS_CHP,              // 0x2F18
   CC1120_FS_DIVTWO,           // 0x2F19
   CC1120_FS_DSM1,             // 0x2F1A
   CC1120_FS_DSM0,             // 0x2F1B
   CC1120_FS_DVC1,             // 0x2F1C
   CC1120_FS_DVC0,             // 0x2F1D
   CC1120_FS_LBI,              // 0x2F1E
   CC1120_FS_PFD,              // 0x2F1F
   CC1120_FS_PRE,              // 0x2F20
   CC1120_FS_REG_DIV_CML,      // 0x2F21
   CC1120_FS_SPARE,            // 0x2F22
   CC1120_FS_VCO4,             // 0x2F23
   CC1120_FS_VCO3,             // 0x2F24
   CC1120_FS_VCO2,             // 0x2F25
   CC1120_FS_VCO1,             // 0x2F26
   CC1120_FS_VCO0,             // 0x2F27
   CC1120_GBIAS6,              // 0x2F28
   CC1120_GBIAS5,              // 0x2F29
   CC1120_GBIAS4,              // 0x2F2A
   CC1120_GBIAS3,              // 0x2F2B
   CC1120_GBIAS2,              // 0x2F2C
   CC1120_GBIAS1,              // 0x2F2D
   CC1120_GBIAS0,              // 0x2F2E
   CC1120_IFAMP,               // 0x2F2F
   CC1120_LNA,                 // 0x2F30
   CC1120_RXMIX,               // 0x2F31
   CC1120_XOSC5,               // 0x2F32
   CC1120_XOSC4,               // 0x2F33
   CC1120_XOSC3,               // 0x2F34
   CC1120_XOSC2,               // 0x2F35
   CC1120_XOSC1,               // 0x2F36
   CC1120_XOSC0,               // 0x2F37
   CC1120_ANALOG_SPARE,        // 0x2F38
   CC1120_PA_CFG3,             // 0x2F39
   CC1120_IRQ0M,               // 0x2F3F
   CC1120_IRQ0F                // 0x2F40
} registerSetting_t;


#endif //REGADDRESS

#include "mqttlib.h"

MQTTLib::MQTTLib()
{
    aliveInterval_ = 20;
    cleanSession_ = 1;
    timeout_ = 10000;
}

MQTTLib::~MQTTLib()
{
    if (MQTTClient_disconnect(client_, timeout_) == MQTTCLIENT_SUCCESS)
    {
        MQTTClient_destroy(&client_);
        printf("mqtt is destroyed\n");
    }
}

void MQTTLib::initMQTT(MQTTClient_connectionLost *cl, MQTTClient_messageArrived *ma,
                       MQTTClient_deliveryComplete *dc)
{
    char errmsg[128];

    connectOpts_ = MQTTClient_connectOptions_initializer;
    pubmsg_ = MQTTClient_message_initializer;

    MQTTClient_create(&client_, serverAddress_.c_str(), clientID_.c_str(),
                      MQTTCLIENT_PERSISTENCE_NONE, NULL);

    connectOpts_.keepAliveInterval = aliveInterval_;
    connectOpts_.cleansession = cleanSession_;
    connectOpts_.username = username_.c_str();
    connectOpts_.password = password_.c_str();

    MQTTClient_setCallbacks(client_, NULL, cl, ma, dc);

    if ((rc_ = MQTTClient_connect(client_, &connectOpts_)) != MQTTCLIENT_SUCCESS)
    {
        snprintf(errmsg, (sizeof errmsg) - 1, "Failed to connect, return code %d\n", rc_);
        LogLog::getLogLog()->error(LOG4CPLUS_TEXT(errmsg));
        throw(Exception(errmsg));
        printf("Failed to connect, return code %d\n", rc_);
    }
}
int MQTTLib::initSubscribe(const char *_topic, int _qos)
{
    lock_guard<mutex> local_lock(mutex_);
    return MQTTClient_subscribe(client_, _topic, _qos);
}

int MQTTLib::stopSubscribe(const char *_topic)
{
    lock_guard<mutex> local_lock(mutex_);
    return MQTTClient_unsubscribe(client_, _topic);
}

void MQTTLib::pubMQTT(uint8_t *_payload, int _lenght, const char *_topic, int _qos, int _retained)
{
    // lock_guard<mutex> local_lock(mutex_);

    pubmsg_.payload = _payload;
    pubmsg_.payloadlen = _lenght;
    pubmsg_.qos = _qos;
    pubmsg_.retained = _retained;
    MQTTClient_publishMessage(client_, _topic, &pubmsg_, &token_);
    // MQTTClient_waitForCompletion(client_, token_, 1000L);
    // printf("Message '%s' with delivery token %d delivered\n", _payload, token_);
}

int MQTTLib::stopMQTT()
{
    lock_guard<mutex> local_lock(mutex_);

    rc_ = MQTTClient_disconnect(client_, timeout_);
    MQTTClient_destroy(&client_);

    return rc_;
}

void MQTTLib::setServerAddr(const char *_serverAddress)
{
    serverAddress_.assign(_serverAddress);
}

void MQTTLib::setClientID(const char *_clientID)
{
    clientID_.assign(_clientID);
}

void MQTTLib::setUsername(const char *_username)
{
    username_.assign(_username);
}

void MQTTLib::setPassword(const char *_password)
{
    password_.assign(_password);
}

void MQTTLib::setAliveInterval(int _interval)
{
    aliveInterval_ = _interval;
}

void MQTTLib::setCleanSession(int _session)
{
    cleanSession_ = _session;
}

void MQTTLib::setTimeout(int _timeout)
{
    timeout_ = _timeout;
}

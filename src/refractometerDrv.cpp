#include "refractometerDrv.h"
#include "syslog.h"

void refractometerDrv::serialInit()
{
    if ((fd = serialOpen("/dev/ttyUSBPort1", 4800)) < 0)
    {
        fprintf(stderr, "Unable to open serial device: %s\n", strerror(errno));
        printf("Unable to open serial device:\n");
    }

    // Uncomment when main program doesn't use wiringPiSetups
    // if (wiringPiSetup() == -1)
    // {
    //     fprintf(stdout, "Unable to start wiringPi: %s\n", strerror(errno));
    //     return;
    // }
}

void refractometerDrv::readConcentrationSingle(float *pConcentration, float *pTemperature)
{
    if ((fd = serialOpen("/dev/ttyUSBPort1", 4800)) < 0)
    {
        fprintf(stderr, "Unable to open serial device: %s\n", strerror(errno));
        printf("Unable to open serial device:\n");
        syslog(LOG_NOTICE, "Unable to open serial device\n");
    }
    else
    {
        unsigned char cmdReadSingle[] = {0x40, 0x31, 0x33, 0x30, 0x32, 0x0D}; // @1302(0x0D)

        for (int i = 0; i < sizeof(cmdReadSingle); i++)
            serialPutchar(fd, cmdReadSingle[i]);
        delay(2500);
        a = 0;
        if (serialDataAvail(fd))
        {
            while (serialDataAvail(fd))
            {
                dataBuff[a] = serialGetchar(fd);
                delay(100);
                a++;
            }

            /* Reading Data */
            // for (int i = 0; i < a; i++)
            //     printf("0x%X ", dataBuff[i]);
            // printf("\nFinished Reading\n");
            // printf("Processing Data\n");

            /* Concentration */
            char pTemp[10];
            int g = 0;
            if (dataBuff1[11] == 0x2E)
            {
                for (int i = 12; i < 15; i++)
                {
                    pTemp[g] = dataBuff[i];
                    syslog(LOG_NOTICE, "Concentrate data[%d] = 0x%X", g, pTemp[g]);
                    g++;
                }
            }
            else if (dataBuff1[11] == 0x00)
            {
                for (int i = 12; i < 15; i++)
                {
                    pTemp[g] = dataBuff[i];
                    syslog(LOG_NOTICE, "Concentrate data[%d] = 0x%X", g, pTemp[g]);
                    g++;
                }
            }
            else
            {
                for (int i = 11; i < 15; i++)
                {
                    pTemp[g] = dataBuff[i];
                    syslog(LOG_NOTICE, "Concentrate data[%d] = 0x%X", g, pTemp[g]);
                    g++;
                }
            }
            pTemp[g] = '\0';
            float z = atof(pTemp);
            syslog(LOG_NOTICE, "Concentrate value : %3.1f", z);
            *pConcentration = z;

            /* Temperatures */
            char pTemp1[10];
            int h = 0;
            for (int i = 6; i < 10; i++)
            {
                pTemp1[h] = dataBuff[i];
                h++;
            }
            pTemp1[h] = '\0';
            float y = atof(pTemp1);
            syslog(LOG_NOTICE, "Temperature value : %3.1f", y);
            *pTemperature = y;
        }
        else
        {
            printf("Sensor Not Respond\n");
        }
    }
}

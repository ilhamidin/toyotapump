#include "phtempdriver.h"

void phtempdriver::serialInit()
{
    if ((fd = serialOpen("/dev/ttyS2", 9600)) < 0)
    {
        fprintf(stderr, "Unable to open serial device: %s\n", strerror(errno));
        printf("Unable to open serial device:\n");
        return;
    }

    // Uncomment when main program doesn't use wiringPiSetups
    // if (wiringPiSetup() == -1)
    // {
    //     fprintf(stdout, "Unable to start wiringPi: %s\n", strerror(errno));
    //     return;
    // }
}

void phtempdriver::readPhTempSingle(float *ph, float *temperature, bool *pCrc)
{
    /* Asking Data from Sensor */
    unsigned char singleReadCmd[] = {devId, 3, 0, 0, 0, 2, 196, 11}; // 0x03, 0x00, 0x00, 0x00, 0x03, 0x05, 0xCB
    *ph = 0;
    *temperature = 0;
    for (int i = 0; i < sizeof(singleReadCmd); i++)
        serialPutchar(fd, singleReadCmd[i]);
    delay(1000);
    a = 0;
    // wait(2);
    if (serialDataAvail(fd))
    {
        while (serialDataAvail(fd))
        {
            dataBuff[a] = serialGetchar(fd);
            delay(100);
            a++;
        }

        /* Reading Data */
        // for (int i = 0; i < a; i++)
        //     printf("0x%X ", dataBuff[i]);
        // printf("Finished Reading\n");
        // printf("Processing Data\n");

        /* CRC Check */
        buff = dataBuff[8] << 8;
        buff1 = buff | dataBuff[7];
        // printf("\ncrc from sensor = 0x%X\n", buff1);
        unsigned char b = a - 2;
        for (int i = 0; i < b; i++)
            dataBuff1[i] = dataBuff[i];
        crcBuff = this->modRtuCrc(dataBuff1, b);
        // printf("\ncrc from calculation = 0x%X\n", crcBuff);
        if (crcBuff == buff1)
        {
            *pCrc = true;
            // printf("CRC Check OK\n");

            /* Temperature Calculation */
            if (dataBuff[3] == 0xFF) // In Case Minus Temperature
            {
                buff = dataBuff[3] << 8;
                buff1 = buff | dataBuff[4];
                tempBuff1 = buff1 - 65535;
            }
            else
            {
                tempBuff = dataBuff[3] * 256;
                tempBuff1 = tempBuff + dataBuff[4];
            }

            // printf("dataBuff[3] value is = 0x%X\n", dataBuff[3]);
            // printf("tempBuff1 value is = %f\n", tempBuff1);
            *temperature = tempBuff1 / 10;
            // printf("Temperature = %3.1f *C\n", result);

            // Ph Calculation
            phBuff = dataBuff[5] * 256;
            // printf("dataBuff[5] value is = 0x%X\n", dataBuff[5]);
            phBuff1 = phBuff + dataBuff[6];
            // printf("phBuff1 value is = %f\n", phBuff1);
            *ph = phBuff1 / 10;
            // printf("Ph Concentration = %3.1f \n", result);
        }
        else
        {
            *pCrc = false;
            // printf("CRC Check NOT OK!!!!!\n");
        }
    }
    else
    {
        printf("Sensor Not Respond\n");
    }
}

void phtempdriver::readPhTempBurst(float *ph, float *temperature, bool *pCrc, int *pIteration)
{
    float dataPh[10];
    float dataTemp[10];
    float buff;
    int a = 0;
    for (int i = 0; i < *pIteration; i++)
    {
        this->readPhTempSingle(ph, temperature, pCrc);
        dataPh[i] = *ph;
        dataTemp[i] = *temperature;
        a++;
    }

    // for (int i = 0; i < a; i++)
    // {
    //     printf("Ph[%d] value is = %3.1f\n", i, dataPh[i]);
    //     printf("Temperature[%d] value is = %3.1f\n", i, dataTemp[i]);
    // }

    // printf("Read Finished\n");
}

uint16_t phtempdriver::modRtuCrc(unsigned char *buf, int len)
{
    uint16_t crc = 0xFFFF;
    for (int pos = 0; pos < len; pos++)
    {
        crc ^= (uint16_t)buf[pos]; // XOR byte into least sig. byte of crc

        for (int i = 8; i != 0; i--)
        { // Loop over each bit
            if ((crc & 0x0001) != 0)
            {              // If the LSB is set
                crc >>= 1; // Shift right and XOR 0xA001
                crc ^= 0xA001;
            }
            else           // Else LSB is not set
                crc >>= 1; // Just shift right
        }
    }
    // Note, this number has low and high bytes swapped, so use it accordingly (or swap bytes)
    return crc;
}
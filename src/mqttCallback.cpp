#include "mqttCallback.h"

MQTTCallback *pMQTTClient;

void setClient(MQTTCallback *client)
{
	pMQTTClient = client;
}

void delivered(void *context, MQTTClient_deliveryToken dt)
{
    pMQTTClient->publishDone(dt);
}

int msgarrvd(void *context, char *topicName, int topicLen, MQTTClient_message *message)
{
    string msg, topic;
    
    printf("Message arrived\n");
    topic = topicName;
    msg = (char*)message->payload;

    pMQTTClient->recvMessage(msg, topic);

    MQTTClient_freeMessage(&message);
    MQTTClient_free(topicName);

    return 1;   // must be 1
}

void connlost(void *context, char *cause)
{
    pMQTTClient->connectionLost();
}
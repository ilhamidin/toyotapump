#include <PollingSignal.hpp>
#include <signal.h>

map<int,PollingSignalClient *> PollingSignal::handlers;
PollingSignal::PollingSignal()
{
    handlers.clear();
}

PollingSignal::~PollingSignal()
{
    handlers.clear();
}
//#include <iostream>
void PollingSignal::realPollingSignalHdlr(int signum)
{
    PollingSignalClient *pHandlerData = handlers[signum];
    pHandlerData->execPoll();
}

void PollingSignal::registerHandler(int signum,PollingSignalClient *handler)
{
    struct sigaction sa;
    sa.sa_handler = PollingSignal::realPollingSignalHdlr;
    sigemptyset(&sa.sa_mask);
    sa.sa_flags = 0;
    sa.sa_flags |= SA_RESTART;
    if (sigaction(signum, &sa, 0) > 0) {
         // exception
    }
    handlers[signum] = handler;

}

#include <cstdio>
#include <stdlib.h> // abs function
#include <unistd.h> // printf function
#include <array>
#include <sys/time.h> // time function
#include "cJSON.h"    // cJSON function
#include <string.h>
#include <random> // Generate Rand Number
#include <ctime>  // Generate Rand Number

/* TimerCpp */
#include "timercpp.h"
/* Data Polling Signal */
#include "PollingSignal.hpp"
#include <signal.h>
/* GetNumber */
#include "getnumber.hpp"
/* MQTT */
#include "mqttDriver.h"
/* Ph&Temp Lib */
#include "phtempdriver.h"
/* Refractometer Lib */
#include "refractometerDrv.h"
/* Cc1120 Library */
// #include "cc1120.hpp"
// #include "cc1120_regaddr.hpp"

/* wiringPi */
extern "C"
{
#include <wiringPi.h>
#include <wiringPiSPI.h>
}

/* Using WiringPi Pin Definition */
#define PumpCNC1 22
#define PumpCNC2 23
#define PumpAquaBidest 24
#define PumpOut 25
#define LevelSensor 1

using namespace std;

/* Status Variable */
int status; // 0 = LevelSensor Gak Deteksi Air; 1 = LevelSensor Deteksi Air;
/* MQTT Variable */
string pubTopicAlarm = "alarm.data";
string pubTopicPerformance = "perfmeasurement.data";
string subTopic = "comment.data";
int alarmObjId[] = {0, 1, 2, 6};     // AlarmObjId
int counterIdPh[] = {0, 3, 6, 15};   // counterId untuk Ph
int counterIdTemp[] = {0, 1, 4, 13}; // counterId untuk Temperature
int counterIdConc[] = {0, 2, 5, 14}; // counterId untuk Konsentrasi
/* System Variable */
int timeOut;
int timeOutCNC1 = 120; // TimeOut Pengisian AquaBidest
int timeOutCNC2 = 120; // TimeOut Pengisian Coolant
int timeOutAquaBidest = 120;
int timeOutFlush = 140; // Waktu untuk pengosongan air dari penuh hingga habis (seconds)
unsigned char sendRecvTimeOut;
bool bInterrupt = false;
bool pumpAlarmStatus;
int i;
/* PhTempSensor Variable */
float ph;
float temperature;
bool crcCheck;
/* ConcentrationSensor Variable */
float concentration[25];
/* Date & Time variable */
static int secondsLast = 99;
char timeString[128];
timeval curTime;
/* cJson variable */
int alarmBuff[3][50];
struct alarm_t
{
    string alarmId;
    int objectId;
    string notes;
} jsonAlarmVariable[50];
/* Alarm Variable */
uint64_t numGenerated;

/* Function Object */
mqttDriver mqttDrv;
phtempdriver sensorPhTemp;
refractometerDrv sensorConcentration;

class PollClient : public PollingSignalClient
{
public:
    virtual void execPoll()
    {
        pumpCNC1();
        pumpOut();
        pumpAquaBidest();
        pumpOut();
        pumpCNC2();
        pumpOut();
        pumpAquaBidest();
        pumpOut();

        /* Set All Relay OFF */
        digitalWrite(PumpAquaBidest, HIGH);
        digitalWrite(PumpOut, HIGH);
        digitalWrite(PumpCNC1, HIGH);
        digitalWrite(PumpCNC2, HIGH);
    };

    void pumpOut()
    {
        /* Buka Pompa Flush */
        syslog(LOG_NOTICE, "Flush Pump ON\n");
        digitalWrite(PumpOut, LOW);
        /* Selama timeOutFlush */
        sleep(timeOutFlush);
        /* Matiin Pompa Flush */
        syslog(LOG_NOTICE, "Flush Pump OFF\n");
        digitalWrite(PumpOut, HIGH);
    }

    void pumpAquaBidest()
    {
        /* Nyalain Pompa AquaBidest */
        syslog(LOG_NOTICE, "AquaBidest Pump ON\n");
        digitalWrite(PumpAquaBidest, LOW);
        status = 0;
        i = 3;
        syslog(LOG_NOTICE, "Waiting\n");
        timeOut = timeOutAquaBidest;
        bInterrupt = false;
        timoutWaterFilled();
        if (status == 1)
        {
            /* Matiin Pompa AquaBidest */
            syslog(LOG_NOTICE, "Sensor Level Detected\n");
            syslog(LOG_NOTICE, "AquaBidest Pump OFF\n");
            digitalWrite(PumpAquaBidest, HIGH);
        }
        else
        {
            /* TimeOut Reached */
            syslog(LOG_NOTICE, "TimeOut Reached\n");
            syslog(LOG_NOTICE, "AquaBidest Pump OFF\n");
            digitalWrite(PumpAquaBidest, HIGH);
        }
    }

    void pumpCNC1()
    {
        float concen = 0;
        float temperature1 = 0;

        /* Nyalain Pompa CNC1 */
        syslog(LOG_NOTICE, "CNC1 Pump ON\n");
        digitalWrite(PumpCNC1, LOW);
        status = 0;
        i = 1;
        syslog(LOG_NOTICE, "Waiting\n");
        timeOut = timeOutCNC1;
        bInterrupt = false;
        timoutWaterFilled();
        if (status == 1)
        {
            /* Matiin Pompa CNC1 */
            syslog(LOG_NOTICE, "Sensor Level Detected\n");
        }
        else
        {
            /* TimeOut Reached */
            syslog(LOG_NOTICE, "TimeOut Reached\n");
            /* Send Alarm */
            getDateTime();
            if (pumpAlarmStatus == false)
            {
                // JournalCtl Log
                syslog(LOG_NOTICE, "Alarm in pump CNC %d triggered\n", i);
                // Get Number
                string strNumGen = getNumber();
                // Alarm Variable
                string severity = "CRITICAL";
                string description = "Pump Malfunction";
                // Save Alarm Variable to Global Variable
                jsonAlarmVariable[i].alarmId = strNumGen;
                jsonAlarmVariable[i].objectId = alarmObjId[i];
                jsonAlarmVariable[i].notes = description;
                jsonAlarmSetClear(strNumGen, alarmObjId[i], severity, description, timeString);
                pumpAlarmStatus = true;
            }
            /* ---------- */
        }

        /* Ukur sensor Ph, dan Konsentrasi */
        sensorPhTemp.serialInit();
        sensorPhTemp.readPhTempSingle(&ph, &temperature, &crcCheck);
        sensorConcentration.readConcentrationSingle(&concen, &temperature1);

        syslog(LOG_NOTICE, "CNC1 Pump OFF\n");
        digitalWrite(PumpCNC1, HIGH);

        /* Kirim Hasil Pengukuran Ke Server */
        getDateTime();
        jsonWritePerformance(counterIdPh[i], timeString, ph);
        jsonWritePerformance(counterIdTemp[i], timeString, temperature1);
        jsonWritePerformance(counterIdConc[i], timeString, concen);
    }

    void pumpCNC2()
    {
        float concen = 0;
        float temperature1 = 0;

        /* Nyalain Pompa CNC2 */
        syslog(LOG_NOTICE, "CNC2 Pump ON\n");
        digitalWrite(PumpCNC2, LOW);
        status = 0;
        i = 2;
        syslog(LOG_NOTICE, "Waiting\n");
        timeOut = timeOutCNC2;
        bInterrupt = false;
        timoutWaterFilled();
        if (status == 1)
        {
            /* Matiin Pompa CNC2 */
            syslog(LOG_NOTICE, "Sensor Level Detected\n");
        }
        else
        {
            /* TimeOut Reached */
            syslog(LOG_NOTICE, "TimeOut Reached\n");
            getDateTime();
            if (pumpAlarmStatus == false)
            {
                // JournalCtl Log
                syslog(LOG_NOTICE, "Alarm in pump CNC %d triggered\n", i);
                // Get Number
                string strNumGen = getNumber();
                // Alarm Variable
                string severity = "CRITICAL";
                string description = "Pump Malfunction";
                // Save Alarm Variable to Global Variable
                jsonAlarmVariable[i].alarmId = strNumGen;
                jsonAlarmVariable[i].objectId = alarmObjId[i];
                jsonAlarmVariable[i].notes = description;
                jsonAlarmSetClear(strNumGen, alarmObjId[i], severity, description, timeString);
                pumpAlarmStatus = true;
            }
            /* ---------- */
        }
        /* Ukur sensor Ph, dan Konsentrasi */
        sensorPhTemp.serialInit();
        sensorPhTemp.readPhTempSingle(&ph, &temperature, &crcCheck);
        sensorConcentration.readConcentrationSingle(&concen, &temperature1);

        syslog(LOG_NOTICE, "CNC2 Pump OFF\n");
        digitalWrite(PumpCNC2, HIGH);

        /* Kirim Hasil Pengukuran Ke Server */
        getDateTime();
        jsonWritePerformance(counterIdPh[i], timeString, ph);
        jsonWritePerformance(counterIdTemp[i], timeString, temperature1);
        jsonWritePerformance(counterIdConc[i], timeString, concen);
    }

    void timoutWaterFilled()
    {
        sendRecvTimeOut = 0;
        while (status != 1 && sendRecvTimeOut < timeOut)
        {
            sleep(1);
            sendRecvTimeOut++;
        }
    }

    void getDateTime()
    {
        gettimeofday(&curTime, NULL);
        if (secondsLast == curTime.tv_sec)
        {
            syslog(LOG_NOTICE, "Date&Time Error");
            return;
        }
        secondsLast = curTime.tv_sec;
        strftime(timeString, 80, "%Y-%m-%dT%H:%M:%SZ", gmtime(&curTime.tv_sec));
    }

    string getNumber()
    {
        bool a = getnumber(&numGenerated);
        if (a == 1)
        {
            string b = to_string(numGenerated);
            return b;
        }
        else
        {
            syslog(LOG_NOTICE, "Number generator failed\n");
        }
    }

    void jsonWritePerformance(int counterId, string dateTime, float value)
    {
        char *out;
        cJSON *root, *id;
        // Create Object
        root = cJSON_CreateObject();

        // counter object
        cJSON_AddItemToObject(root, "counter", id = cJSON_CreateObject());
        cJSON_AddNumberToObject(id, "id", counterId);
        cJSON_AddStringToObject(root, "measurementTime", dateTime.c_str());
        cJSON_AddNumberToObject(root, "measurementValue", value);

        // print everything
        out = cJSON_Print(root);
        mqttSend(out, pubTopicPerformance);
        syslog(LOG_NOTICE, "%s\n", out);
        free(out);

        // free all objects under root and root itself
        cJSON_Delete(root);
    }

    void jsonAlarmSetClear(string alarmId, int objectId, string alarmType, string description, string dateTime)
    {
        char *out;
        cJSON *root, *id;
        // Create Object
        root = cJSON_CreateObject();

        // Alarm Object
        cJSON_AddStringToObject(root, "id", alarmId.c_str());
        cJSON_AddItemToObject(root, "alarmObj", id = cJSON_CreateObject());
        cJSON_AddNumberToObject(id, "id", objectId);
        cJSON_AddStringToObject(root, "type", "Security");
        cJSON_AddStringToObject(root, "severity", alarmType.c_str());
        cJSON_AddStringToObject(root, "description", description.c_str());
        cJSON_AddStringToObject(root, "actTime", dateTime.c_str());

        // print everything
        out = cJSON_Print(root);
        mqttSend(out, pubTopicAlarm);
        syslog(LOG_NOTICE, "%s\n", out);
        free(out);

        // free all objects under root and root itself
        cJSON_Delete(root);
    }

    void mqttSend(string payload, string pubtopic)
    {
        mqttDrv.pMQTT_->pubMQTT((uint8_t *)payload.c_str(), payload.length(), pubtopic.c_str(), 0, 0);
    }
};

void myInterrupt0(void)
{
    piLock(1);
    // syslog(LOG_NOTICE, "Interrrupt Triggered\n");
    if (bInterrupt == false)
    {
        if (status == 0)
        {
            status = 1;
            bInterrupt = true;
        }
        else if (status == 1)
        {
            status = 0;
            bInterrupt = true;
        }
    }
    piUnlock(1);
}

int main()
{
    syslog(LOG_NOTICE, "Rev1 Program Loaded\n");
    syslog(LOG_NOTICE, "Init Started\n");
    // wiringPiSetupGpio();
    wiringPiSetup();
    syslog(LOG_NOTICE, "Init Finished\n");
    // pinMode(LevelSensor, INPUT);
    pinMode(PumpAquaBidest, OUTPUT);
    pinMode(PumpOut, OUTPUT);
    pinMode(PumpCNC1, OUTPUT);
    pinMode(PumpCNC2, OUTPUT);

    digitalWrite(PumpAquaBidest, HIGH);
    digitalWrite(PumpOut, HIGH);
    digitalWrite(PumpCNC1, HIGH);
    digitalWrite(PumpCNC2, HIGH);

    /* Interrupt Template Test Code */
    // while (1)
    // {
    //     int a = digitalRead(LevelSensor);
    //     syslog(LOG_NOTICE, "DigitalIn value is %d\n", a);
    //     if (a)
    //     {
    //         syslog(LOG_NOTICE, "Water Detected !!! \n");
    //         digitalWrite(PumpAquaBidest, HIGH);
    //         digitalWrite(PumpOut, HIGH);
    //         digitalWrite(PumpCoolant, HIGH);
    //     }
    //     else
    //     {
    //         syslog(LOG_NOTICE, "Reading ...\n");
    //         digitalWrite(PumpAquaBidest, LOW);
    //         digitalWrite(PumpOut, LOW);
    //         digitalWrite(PumpCoolant, LOW);
    //     }
    //     delay(5000);
    // }

    /* Delay After BootUp */
    sleep(120); // Wait for Modem to Connect internet first

    // /*  MQTT Init */
    syslog(LOG_NOTICE, "MQTT Init\n");
    try
    {
        mqttDrv.pMQTT_ = new MQTTLib();
        /* MQTT Cloud Broker */
        // mqttDrv.pMQTT_->setServerAddr("tcp://soldier.cloudmqtt.com:12965");
        // mqttDrv.pMQTT_->setUsername("tmxdbxjc");
        // mqttDrv.pMQTT_->setPassword("RGmJSoxxG1Zg");
        /* Server Toni */
        mqttDrv.pMQTT_->setServerAddr("tcp://satunol-dev.ddns.net:1883");
        mqttDrv.pMQTT_->setUsername("");
        mqttDrv.pMQTT_->setPassword("");

        mqttDrv.pMQTT_->setClientID("toyotaPump.Gateway");
        mqttDrv.pMQTT_->initMQTT(connlost, msgarrvd, delivered);
        // mqttDrv.pMQTT_->initSubscribe(subTopic.c_str(), 1);
    }
    catch (Exception &e)
    {
        syslog(LOG_NOTICE, "Error MQTT Init\n");
        cerr << e.getMessage() << '\n';
    }

    /* Init ISR */
    syslog(LOG_NOTICE, "ISR Init\n");
    wiringPiISR(LevelSensor, INT_EDGE_BOTH, &myInterrupt0);

    /* Data Polling Signal */
    PollingSignal pollSignal;
    pollSignal.registerHandler(SIGUSR1, new PollClient());

    while (true)
    {
        sleep(2);
    }
}

#include "cc1120.hpp"
#include <iostream>
#include <cstdio>
#include <cstring>
#include <stdlib.h>
#include <sys/mman.h>
#include <syslog.h>

extern "C"
{
#include <wiringPi.h>
#include <wiringPiSPI.h>
    // #include <wiringPi_bpi.h>
}

#include "cc1120_regaddr.hpp"

radiocc1120::radiocc1120()
{
}

radiocc1120::~radiocc1120()
{
}

// GUI Function
int radiocc1120::getTxPower()
{
    int tx_buff = this->readRegSingle(StdRegSpace, CC112X_PA_CFG2);
    tx_buff = tx_buff & 63;
    int tx_buff_result = (((tx_buff + 1) / 2) - 18);
    return tx_buff_result;
}

void radiocc1120::setSniffMode()
{
    this->writeRegSingle(CmdStrobe, CC112X_SWOR, 0x00);
}

void radiocc1120::setSyncWord(unsigned int value)
{
    syncWord = value;
}

void radiocc1120::configSyncWord()
{
    int syncWord3 = syncWord >> 24;
    int syncWord2 = (syncWord & 0x00FF0000) >> 16;
    int syncWord1 = (syncWord & 0x0000FF00) >> 8;
    int syncWord0 = (syncWord & 0x000000FF);
    this->writeRegSingle(StdRegSpace, syncWord3, CC112X_SYNC3);
    this->writeRegSingle(StdRegSpace, syncWord2, CC112X_SYNC2);
    this->writeRegSingle(StdRegSpace, syncWord1, CC112X_SYNC1);
    this->writeRegSingle(StdRegSpace, syncWord0, CC112X_SYNC0);
}

void radiocc1120::setChipAddress(unsigned char chipAddr)
{
    this->setRegister(CC1120_DEV_ADDR, chipAddr);
}

// Cc1120 Function
void radiocc1120::spiInitCc1120(unsigned char cs, unsigned char rst, unsigned char irq)
{
    unsigned char buff1;
    uint16_t ii;
    ii = 0;

    chipSelectPin = cs;
    chipResetPin = rst;
    chipIrqPin = irq;
    syslog(LOG_NOTICE, "Init Started\n");
    // cout << "Init Started\n";
    // wiringPiSetup();
    wiringPiSetupGpio();
    syslog(LOG_NOTICE, "GPIO setup finished\n");
    // cout << "GPIO setup finished\n";

    pinMode(chipSelectPin, OUTPUT);
    pinMode(chipResetPin, OUTPUT);
    pinMode(chipIrqPin, INPUT);

    digitalWrite(chipSelectPin, HIGH);

    delay(50); // this wait needs in order to prepare the cc1120 chip
    if (wiringPiSPISetup(0, 6000000) < 0)
        printf("SPI Setup failed\n");
    delay(200); // this wait needs for chip to set crystal to be stabilized
    syslog(LOG_NOTICE, "SPI setup finished\n");
    // cout << "SPI setup finished\n";

    delay(20); // Prepare chip before resetting
    this->writeRegSingle(CmdStrobe, CC112X_SRES, 0x00);
    delay(100); // Prepare chip before resetting
    this->writeRegSingle(CmdStrobe, CC112X_SRES, 0x00);
    delayMicroseconds(20); // wait for MISO tobe low after resetting

    do
    {
        buff1 = this->readRegSingle(ExtAddr, CC112X_PARTNUMBER);
        if ((ii++ % 5) == 0)
        {
            // CC112X HW Reset
            digitalWrite(chipResetPin, LOW);
            delay(50);
            digitalWrite(chipResetPin, HIGH);
            // ---------------
            syslog(LOG_NOTICE, "CC1120 HW Reset\n");
            // cout << "CC1120 HW Reset.\r\n";
        }
        if (ii > 15)
        {
            syslog(LOG_NOTICE, "CC1120 can not init\n");
            // cout << "CC1120 can not init.\r\n";
        }
    } while (buff1 != 0x48);
    syslog(LOG_NOTICE, "CC1120 init finished\n");
    // cout << "CC1120 init finished\n";

    /*
        bufferAddr = 0xAF;
        bufferValue = 0x8F;
        bufferResult = 0x00;
        digitalWrite(chipSelectPin, LOW);
        wiringPiSPIDataRW(0, &bufferAddr, 1);
        wiringPiSPIDataRW(0, &bufferValue, 1);
        wiringPiSPIDataRW(0, &bufferResult, 1);
        digitalWrite(chipSelectPin, HIGH);
    */

    buff1 = this->readRegSingle(ExtAddr, CC112X_PARTNUMBER);
    syslog(LOG_NOTICE, "CC112X_PARTNUMBER value is: 0x%X\n", buff1);
    printf("CC112X_PARTNUMBER value is: 0x%X\n", buff1);
}

void radiocc1120::errorHandler()
{
    cout << "Error Occured\n";
}

void radiocc1120::flushRxFifo()
{
    this->writeRegSingle(CmdStrobe, CC112X_SFRX, 0x00);
}

void radiocc1120::flushTxFifo()
{
    this->writeRegSingle(CmdStrobe, CC112X_SFTX, 0x00);
}

int radiocc1120::radioStatusCheck()
{
    int status = this->readRegSingle(CmdStrobe, CC112X_SNOP);
    status = status & 0xF0;
    status = status >> 4;
    if (status == 0)
    { // IDLE
        status = 0;
    }
    if (status == 1)
    { // RX Mode
        status = 1;
    }
    if (status == 2)
    { // TX Mode
        status = 2;
    }
    if (status == 3)
    { // FSTXON (Fast TX ready)
        status = 3;
    }
    if (status == 4)
    { // Calibrate
        status = 4;
    }
    if (status == 5)
    { // Settling
        status = 5;
    }
    if (status == 6)
    { // RX FIFO ERROR
        status = 6;
    }
    if (status == 7)
    { // TX FIFO ERROR
        status = 7;
    }
    return status;
}

int radiocc1120::readRxFirst()
{
    int rxFirst = this->readRegSingle(ExtAddr, CC112X_RXFIRST);
    return rxFirst;
}

int radiocc1120::readRxLast()
{
    int rxLast = this->readRegSingle(ExtAddr, CC112X_RXLAST);
    return rxLast;
}

void radiocc1120::readPacket(unsigned char *pData, int *pLen, char *pRssi, char *pLQI, bool *pCRC)
{
    delayMicroseconds(350);
    unsigned char readReg = 0xFF;
    unsigned char i;
    unsigned char crclqi;
    unsigned char buff;
    unsigned char rxFirst = this->readRxFirst();
    digitalWrite(chipSelectPin, LOW);
    wiringPiSPIDataRW(channel, &readReg, 1);
    buff = rxFirst;
    wiringPiSPIDataRW(channel, &buff, 1);
    unsigned char len = buff;
    for (i = 0; i < len; i++)
    {
        unsigned char buff1 = i + 1;
        wiringPiSPIDataRW(channel, &buff1, 1);
        pData[i] = buff1;
    }
    buff = i + 1;
    wiringPiSPIDataRW(channel, &buff, 1);
    *pRssi = buff;
    i++;
    buff = i;
    wiringPiSPIDataRW(channel, &buff, 1);
    crclqi = buff;
    *pCRC = crclqi & 0x80;
    *pLQI = crclqi & 0x7f;
    *pLen = len;
    digitalWrite(chipSelectPin, HIGH);
}

void radiocc1120::send(unsigned char *pData, int len)
{
    this->writeRegBurst(StdFIFO, pData, len, 0x00);
    this->writeRegSingle(CmdStrobe, CC112X_STX, 0x00);
    delayMicroseconds(550);
}

void radiocc1120::recv()
{
    this->writeRegSingle(CmdStrobe, CC112X_SRX, 0x00);
    delayMicroseconds(550);
}

void radiocc1120::txPacketHandler(unsigned char *pData, unsigned char length, unsigned char chipAddr)
{
    if (length <= 110)
    {
        pData[0] = length;
        pData[1] = chipAddr;
        pData[2] = masterSource;
        int x = this->packetCounter(chipAddr);
        pData[3] = x & 0x000000FF;
        pData[4] = (x & 0x0000FF00) >> 8;
        pData[5] = (x & 0x00FF0000) >> 16;
        pData[6] = (x & 0xFF000000) >> 24;
        for (int i = 7; i <= length; i++)
            pData[i] = rand() % 255;
    }
    else
    {
        cout << "data length exceeded register capacity\n";
    }
}

int radiocc1120::packetCounter(unsigned char devAddr)
{
    return pqtCounter[devAddr]++;
}

// Write/Read SPI function
void radiocc1120::writeRegSingle(RegAcces access, unsigned char Value, unsigned char Addr)
{
    if (access == StdRegSpace)
    {
        if (Addr < 0x2F)
        {
            unsigned char buff = Addr;
            unsigned char buff1 = Value;
            digitalWrite(chipSelectPin, LOW);
            wiringPiSPIDataRW(channel, &buff, 1);
            wiringPiSPIDataRW(channel, &buff1, 1);
            digitalWrite(chipSelectPin, HIGH);
        }
        else
        {
            printf("writeRegSingle StdRegSpace Error in addr = 0x%X\n", Addr);
        }
    }
    else if (access == ExtAddr)
    {
        unsigned char buff = 0x2F;
        digitalWrite(chipSelectPin, LOW);
        wiringPiSPIDataRW(channel, &buff, 1);
        wiringPiSPIDataRW(channel, &Addr, 1);
        wiringPiSPIDataRW(channel, &Value, 1);
        digitalWrite(chipSelectPin, HIGH);
    }
    else if (access == CmdStrobe)
    {
        if (Value >= 0x30 && Value <= 0x3D)
        {
            unsigned char buff = Value;
            digitalWrite(chipSelectPin, LOW);
            wiringPiSPIDataRW(channel, &buff, 1);
            digitalWrite(chipSelectPin, HIGH);
        }
        else
        {
            printf("writeRegSingle CmdSrobe Error in addr = 0x%X\n", Addr);
        }
    }
    else
    {
        printf("writeRegSingle Access Error\n");
    }
}

void radiocc1120::writeRegBurst(RegAcces access, unsigned char *pData, int length, unsigned char Addr)
{
    if (access == StdRegSpace)
    {
        if (Addr < 0x2F)
        {
            unsigned char buff = Addr | RADIO_BURST_ACCESS;
            digitalWrite(chipSelectPin, LOW);
            wiringPiSPIDataRW(channel, &buff, 1);
            for (int i = 0; i < length; i++)
            {
                wiringPiSPIDataRW(channel, &pData[i], 1);
            }
            digitalWrite(chipSelectPin, HIGH);
        }
        else
        {
            printf("writeRegBurst StdRegSpace Error in addr = 0x%X\n", Addr);
        }
    }
    else if (access == ExtAddr)
    {
        unsigned char buff = Addr | RADIO_BURST_ACCESS;
        unsigned char buff1 = 0x2F;
        digitalWrite(chipSelectPin, LOW);
        wiringPiSPIDataRW(channel, &buff1, 1);
        wiringPiSPIDataRW(channel, &buff, 1);
        for (int i = 0; i < length; i++)
        {
            wiringPiSPIDataRW(channel, &pData[i], 1);
        }
        digitalWrite(chipSelectPin, HIGH);
    }
    else if (access == StdFIFO)
    {
        if (length <= 0x7F) // Length not more than 0x7F or 128
        {
            unsigned char buff = 0x7F;
            unsigned char buff1 = length;
            unsigned char buff2;
            digitalWrite(chipSelectPin, LOW);
            wiringPiSPIDataRW(channel, &buff, 1);
            wiringPiSPIDataRW(channel, &buff1, 1);
            for (int i = 1; i <= length; i++)
            {
                buff2 = pData[i];
                wiringPiSPIDataRW(channel, &buff2, 1);
            }
            digitalWrite(chipSelectPin, HIGH);
        }
        else
        {
            printf("writeRegBurst StdFIFO Error in length, with length value is = 0x%X\n", length);
        }
    }
}

int radiocc1120::readRegSingle(RegAcces access, unsigned char Addr)
{
    if (access == StdRegSpace)
    {
        if (Addr < 0x2F)
        {
            bufferResult = 0x00;
            unsigned char buff = Addr | RADIO_READ_ACCESS;
            digitalWrite(chipSelectPin, LOW);
            wiringPiSPIDataRW(channel, &buff, 1);
            wiringPiSPIDataRW(channel, &bufferResult, 1);
            digitalWrite(chipSelectPin, HIGH);
            return bufferResult;
        }
        else
        {
            printf("readRegSingle StdRegSpace Error in addr = 0x%X\n", Addr);
        }
    }
    else if (access == ExtAddr)
    {
        bufferResult = 0x00;
        unsigned char buff = 0xAF;
        digitalWrite(chipSelectPin, LOW);
        wiringPiSPIDataRW(channel, &buff, 1);
        wiringPiSPIDataRW(channel, &Addr, 1);
        wiringPiSPIDataRW(channel, &bufferResult, 1);
        digitalWrite(chipSelectPin, HIGH);
        return bufferResult;
    }
    else if (access == CmdStrobe)
    {
        bufferResult = Addr;
        digitalWrite(chipSelectPin, LOW);
        wiringPiSPIDataRW(channel, &bufferResult, 1);
        digitalWrite(chipSelectPin, HIGH);
        return bufferResult;
    }
    else
    {
        printf("readRegSingle Accsess Error");
    }
}

void radiocc1120::readRegBurst(unsigned char *pData, int rxFirst, int rxLast)
{
    unsigned char buff = 0xFF;
    int i = 0;
    digitalWrite(chipSelectPin, LOW);
    wiringPiSPIDataRW(channel, &buff, 1); // read RX FIFO
    for (unsigned char n = rxFirst; n <= rxLast; n++)
    {
        wiringPiSPIDataRW(channel, &n, 1); // read RX FIFO
        // serial->printf("data%d value is = 0x%X\n", i, pData[i]);
        pData[i] = n;
        i++;
    }
    digitalWrite(chipSelectPin, HIGH);
}

// Write/Read Register
void radiocc1120::setRegister(unsigned short int addr, unsigned char value)
{
    unsigned char addrBuff = addr >> 8;
    addrBuff &= 0xFF;
    unsigned char extAddr = 0x2F;
    digitalWrite(chipSelectPin, LOW);
    if (addrBuff == 0x00)
    {
        unsigned char buff = addr & 0xFF;
        wiringPiSPIDataRW(channel, &buff, 1);
    }
    else if (addrBuff == 0x2F)
    {
        unsigned char buff = addr & 0xFF;
        wiringPiSPIDataRW(channel, &extAddr, 1);
        wiringPiSPIDataRW(channel, &buff, 1);
    }
    else
    {
        this->errorHandler();
        printf("setRegister error in addrbuff = 0x%X\n", addr);
    }
    wiringPiSPIDataRW(channel, &value, 1);
    digitalWrite(chipSelectPin, HIGH);
}

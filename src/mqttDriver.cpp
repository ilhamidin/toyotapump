#include "mqttDriver.h"
#include <stdio.h>

mqttDriver::mqttDriver()
{
    setClient(this);
}

mqttDriver::~mqttDriver()
{
    delete pMQTT_;
}

void mqttDriver::publishDone(MQTTClient_deliveryToken dt)
{
    syslog(LOG_NOTICE, "Publish Done\n");
    printf("Publish done %d\n", dt);
}

void mqttDriver::recvMessage(const string _message, const string _topic)
{
    syslog(LOG_NOTICE, "Massage Received\n");
    printf("message: %s\n", _message.c_str());
}

void mqttDriver::connectionLost()
{
    syslog(LOG_NOTICE, "Connection Lost\n");
    printf("Connection lost\n");
}
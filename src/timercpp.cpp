#include "timercpp.h"

void Timer::setTimeout(std::function<void()> const& function, int delay) {
    this->active = false;
    std::thread t([=]() {
        if(this->active) return;
        std::this_thread::sleep_for(std::chrono::milliseconds(delay));
        if(this->active) return;
        function();
    });
    t.detach();
}

void Timer::setInterval(std::function<void()> const& function, int interval) {
    this->active = false;
    std::thread t([=]() {
        while(true) {
            if(this->active) return;
            std::this_thread::sleep_for(std::chrono::milliseconds(interval));
            if(this->active) return;
            function();
        }
    });
    t.detach();
}

void Timer::stop() {
    this->active = true;
}
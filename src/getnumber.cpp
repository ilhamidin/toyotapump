#include <curl.h>
#include <cstdio>
#include <iostream>
#include "getnumber.hpp"

std::string buffer;

size_t curl_write( void *ptr, size_t size, size_t nmemb, void *stream)
{
  buffer.append((char*)ptr, size*nmemb);
  return size*nmemb;
}

bool getnumber(uint64_t *num)
{
  CURL *curl;
  CURLcode res;
  char str_num[32];

  curl = curl_easy_init();
  buffer.clear();
  if(curl) {
    curl_easy_setopt(curl, CURLOPT_URL, "http://localhost:3001/getnumber/");
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, curl_write);
    res = curl_easy_perform(curl);
    curl_easy_cleanup(curl);
    if(res == CURLE_OK) {
        snprintf(str_num,sizeof str_num,"%s", buffer.c_str());
        *num = atoll(str_num);
        return true;
    }
  }

  return false;

}


